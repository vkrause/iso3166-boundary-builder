# SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: BSD-3-Clause

cmake_minimum_required(VERSION 3.5 FATAL_ERROR)
project(ISO3166BoundaryBuilder VERSION 0.0.1 LANGUAGES NONE)
enable_testing()

include(FeatureSummary)
find_package(ECM REQUIRED NO_MODULE) # for FindIsoCodes
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR} ${CMAKE_SOURCE_DIR}/cmake)

find_package(OsmTools REQUIRED)
find_package(GDALTools REQUIRED)
find_package(IsoCodes)

# We filter the OSM data in multiple stages below:
# (1) retain all elements with an ISO tag, and their dependencies
# (2) drop specific ISO codes we do not want in the result, such as historic ones
# (3) drop all relations not having an ISO tag while ignoring dependencies
#     this removes sub-regions included by (1) before
# (4) rerun (1) to purge the now dangling nodes/ways
#
# Technically, we'd get the same result with omitting (1), but
# at the cost of retaining almost the entire data in step (3) (all nodes/ways)
# making this much more costly than with the extra filter stage.
#
# For conversion to GeoJSON/Shaprefile we also have to convert from the
# format used for filtering (o5m) to the format understood by GDAL (osm.pbf).

#
# ISO 3166-1 boundaries
#
osm_filter(OUTPUT boundaries1-stage0.o5m FILTER --keep=\"ISO3166-1=*\" --drop-node-tags=* --drop-way-tags=*)
# remove reserved codes (see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Exceptional_reservations)
osm_filter(INPUT boundaries1-stage0.o5m OUTPUT boundaries1-stage1.o5m FILTER --drop-relations=\"ISO3166-1=AC or ISO3166-1=CP or ISO3166-1=DG or ISO3166-1=IC or ISO3166-1=TA or ISO3166-1=EU\")
osm_filter(INPUT boundaries1-stage1.o5m OUTPUT boundaries1-stage2.o5m FILTER --keep-relations=\"ISO3166-1=*\" --ignore-dependencies)
osm_filter(INPUT boundaries1-stage2.o5m OUTPUT boundaries1-stage3.o5m FILTER --keep=\"ISO3166-1=*\")
osm_convert(INPUT boundaries1-stage3.o5m OUTPUT boundaries1-stage3.osm.pbf)

ogr2ogr_convert_osm(
    INPUT ${OSM_PLANET_DIR}/boundaries1-stage3.osm.pbf
    OUTPUT ${CMAKE_BINARY_DIR}/iso3166-1-boundaries.shp
    CONFIG ${CMAKE_CURRENT_SOURCE_DIR}/osmconf-iso3166-1.ini
)
ogr2ogr_convert_osm(
    INPUT ${OSM_PLANET_DIR}/boundaries1-stage3.osm.pbf
    OUTPUT ${CMAKE_BINARY_DIR}/iso3166-1-boundaries.geojson
    CONFIG ${CMAKE_CURRENT_SOURCE_DIR}/osmconf-iso3166-1.ini
)


#
# ISO 3166-2 boundaries
#
osm_filter(OUTPUT boundaries2-stage0.o5m FILTER --keep=\"ISO3166-2=*\" --drop-node-tags=* --drop-way-tags=*)
# remove historic boundaries such as the FR-X codes
osm_filter(INPUT boundaries2-stage0.o5m OUTPUT boundaries2-stage1.o5m FILTER --drop-relations=\"boundary=historic or was:boundary=administrative\")
osm_filter(INPUT boundaries2-stage1.o5m OUTPUT boundaries2-stage2.o5m FILTER --keep-relations=\"ISO3166-2=*\" --ignore-dependencies)
osm_filter(INPUT boundaries2-stage2.o5m OUTPUT boundaries2-stage3.o5m FILTER --keep=\"ISO3166-2=*\")
osm_convert(INPUT boundaries2-stage3.o5m OUTPUT boundaries2-stage3.osm.pbf)

ogr2ogr_convert_osm(
    INPUT ${OSM_PLANET_DIR}/boundaries2-stage3.osm.pbf
    OUTPUT ${CMAKE_BINARY_DIR}/iso3166-2-boundaries.shp
    CONFIG ${CMAKE_CURRENT_SOURCE_DIR}/osmconf-iso3166-2.ini
)
ogr2ogr_convert_osm(
    INPUT ${OSM_PLANET_DIR}/boundaries2-stage3.osm.pbf
    OUTPUT ${CMAKE_BINARY_DIR}/iso3166-2-boundaries.geojson
    CONFIG ${CMAKE_CURRENT_SOURCE_DIR}/osmconf-iso3166-2.ini
)


#
# Output targets
#
add_custom_target(shapefile ALL DEPENDS
    ${CMAKE_BINARY_DIR}/iso3166-1-boundaries.shp
    ${CMAKE_BINARY_DIR}/iso3166-2-boundaries.shp
)
add_custom_target(geojson ALL DEPENDS
    ${CMAKE_BINARY_DIR}/iso3166-1-boundaries.geojson
    ${CMAKE_BINARY_DIR}/iso3166-2-boundaries.geojson
)

add_subdirectory(tests)
add_subdirectory(dist)
feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
