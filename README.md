# ISO 3166 Boundary Builder

The ISO 3166 equivalent to the [Timezone Boundary Builder](https://github.com/evansiroky/timezone-boundary-builder/).

This creates Shapefile and GeoJSON extracts from [OSM](https://openstreetmap.org) containing the world's ISO-3166-1/2 boundaries.

## Prerequisites

* osmctools
* `ogr2ogr` from GDAL
* a ton of free SSD space to work with a full OSM database

## Usage

```
cmake -DOSM_PLANET_DIR=<path containing planet-latest.o5m>
make
```

Be patient, this takes time.

## QA

There's a few commands to sanity check the processed data:

`make compare-to-osm` shows the ISO code diff between what was found in OSM and what ended up
in the generated data files. Missing codes point to polygon issues in the OSM data.

`make compare-to-iso-codes` compares the ISO codes in the extracted OSM data to data files
from the iso-codes package. Differences can point to data issues on either side, so this needs
case by case investigation.
