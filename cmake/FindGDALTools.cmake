# SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: BSD-3-Clause

find_program(OGR2OGR_EXECUTABLE ogr2ogr)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GDALTools
    FOUND_VAR GDALTools_FOUND
    REQUIRED_VARS OGR2OGR_EXECUTABLE
)

if (OGR2OGR_EXECUTABLE AND NOT TARGET GDAL::ogr2ogr)
    add_executable(GDAL::ogr2ogr IMPORTED)
    set_target_properties(GDAL::ogr2ogr PROPERTIES IMPORTED_LOCATION ${OGR2OGR_EXECUTABLE})
endif()

set_package_properties(GDALTools PROPERTIES
    DESCRIPTION "GDAL tools"
)

mark_as_advanced(OGR2OGR_EXECUTABLE)


if (TARGET GDAL::ogr2ogr)

# Convert a given OSM file to a supported output format using ogr2ogr
# Arguments:
#   INPUT OSM input file
#   OUTPUT output file
#   CONFIG osmconf.ini file
function(ogr2ogr_convert_osm)
    set(oneValueArgs INPUT OUTPUT CONFIG)
    cmake_parse_arguments(ogr2ogr "" "${oneValueArgs}" "" ${ARGN})

    set(_env_cmd "")
    if (ogr2ogr_CONFIG)
        set(_env_cmd ${CMAKE_COMMAND} -E env OSM_CONFIG_FILE=${ogr2ogr_CONFIG})
    endif()

    add_custom_command(
        OUTPUT ${ogr2ogr_OUTPUT}
        COMMAND ${_env_cmd} ${OGR2OGR_EXECUTABLE} ${ogr2ogr_OUTPUT} ${ogr2ogr_INPUT} multipolygons
        COMMENT "Converting ${ogr2ogr_OUTPUT}"
        DEPENDS ${ogr2ogr_INPUT} ${ogr2ogr_CONFIG}
    )
endfunction()

endif()
